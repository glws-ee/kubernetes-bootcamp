### Global variables

variable "credentials" {
  # NOTE: if using $(file), must be defined in main, not in variables.tfvars for some reason
  default = "/no-credentials/here"
}

variable "project" {
  type        = "string"
  description = "GCP project name:  Note, this can and should be an input from the output of modules/project"
}

variable "region" {
  type        = "string"
  description = "Region to deploy project into"
}

## Required variables for GKE module
variable "name" {}

variable "env" {}

variable "zone" {}

variable "username" {}

variable "password" {}

variable "maximumNodeCount" {
  type        = "string"
  default     = 1
  description = "Maximum instances for GKE Node Pool on auto-scale"
}
