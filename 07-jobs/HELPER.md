# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

## Commands

### Job Lifecycle
```bash
kubectl apply -f 06-jobs/job.yaml

kubectl get job

kubectl delete -f 06-jobs/job.yaml
```

