# Pods

## Goals

* Run a simple pod
* SSH into simple pod
* Create a pod manifest descriptor file
* Create a multi-container pod
* Create an init container
* Clean-up

## Done

This lab is completed when each of the exercises have been completed and NO pods are shown within the private Namespace

* No pods running in namespace

## Navigation
[Namespaces](../01-namespaces) | [Deployments](../03-deployments "03-deployments lab") | [I need help!!](HELPER.md)

---

## Exercises

1. **Run** an `nginx` Pod named "nginx-simple" latest container on port 80
1. SSH into `nginx-simple` container
1. Delete `nginx-simple` Pod
1. Create single nginx pod using Manifest file
    * Name the pod `pod-single`
1. Run an `nginx` and `busybox` container in the same pod
    * Print ANY string to system out every second from the CMD and ARG lines in the `busybox` definition
    * `tail` the logs of the busybox Pod
1. Verify both `nginx-single` and `nginx-multi` STATUS = Running
1. Delete both `nginx-single` and `nginx-multi`
1. Create an `init container` to demonstrate how to wait on a container in the pod