# Overview

This course will walk through Kubernetes 101 and 201 level material covering basics through some advanced scheduling.

### Table of Contents
- [Outline](#Outline)
- [Initial Setup](#initial-setup)
  * [gcloud Utilities](#gcloud-utilities)
  * [GCP Initial Setup and GKE Infrastructure Creation](gcp-initial-setup-and-gke-infrastructure-creation)
- [Connect to Cluster](#connect-to-cluster)
  * [Create kubectl configuration](#create-kubectl-configuration)

# Outline

### Primary Bootcamp
## Links
1. [Overview, kubernetes Architecture](00-kubectl/ "00-kubectl lab")
1. [Namespaces](01-namespaces/ "01-namespaces lab")
1. [Pods](02-pods/ "02-pods lab")
1. [Deployments](03-deployments/ "03-deployments lab")
1. [Services](04-services/ "04-services lab")
1. [Configuration](05-configuration/ "05-configuration lab")
1. [Ingress](06-ingress/ "06-ingress lab")
1. [Jobs](07-jobs/ "07-jobs lab")
1. [Secrets](08-secrets/ "08-secrets lab")
1. [Storage](09-storage/ "09-storage lab")
1. [StatefulSets](10-stateful-apps/ "10-stateful-apps lab")
1. Utilizing CICD Pipelines

# Initial Setup

> The `Initial Setup` is for Developers and Administrators

Developers will be using several tools (`gcloud`, `kubectl`, and a text editor) in their environment.  System administrators or optionally participants will be configuring the GKE instances using `terraform`.

1. Setup Developer's tools (Everyone)
1. Setup Global Infrastructure (Administrators only)

## gcloud utilities

### `gcloud`

Install GCloud utilities.  Follow the instructions to setup `gcloud`
[https://cloud.google.com/deployment-manager/docs/step-by-step-guide/installation-and-setup] 

### Setup gcloud configuration

#### Initialize `gcloud`

```bash
gcloud init
```

#### Explicitly set Project

```bash
gcloud config set project <PROJECT_ID>
```

### Install `kubectl`

Kubectl is the command line tool used to communicate with a Kubernetes cluster.

To install, run:

```bash
gcloud components install kubectl
```

> Next steps is to [Connect to the Cluster](#connect-to-cluster) section (see bottom)

------
------

# Administrators Section

> The setup of GCP and GKE are done once and should be performed by a select group of administrators (or Continuous Integration tool)

## GCP Initial Setup and GKE Infrastructure Creation

### 1. Foundation Layer

The foundation creates a GCP Folder and Project to contain all of the resources used during the workshop.

1. Create and/or modify the `variables.tf` file and BASH environment variables with the instance's specific values

#### Foundation Layer Required Variables

| Variable                    | Example Value            | Var Type             |
| --------------------------- |:------------------------:|:---------------------|
| TF_VAR_billing_account      | 323237273                | Environment          |
| TF_VAR_org_id               | 382838273                | Environment          |
| project                     | some-unique-name         | Terraform            |
| folder_name                 | some-folder              | Terraform            |
| credentials                 | ~/.gcloud/my-creds.json  | Terraform+GCP File   |

#### Create GCP Folder & Project

Terraform will be used to create the Folder and Project inside of GCP.  The `project` will contain the GKE instance created in the [Services Layer](#services-layer) below.

```bash
# Initialize Terraform
user@domain:~$ terraform init
# Validate plan
user@domain:~$ terraform plan
# Build infrastructure (respond with explicit 'yes')
user@domain:~$ terraform apply
```

### 2. Services Layer

The services layer consists of the Cloud services used for the project.  In this case, the project needs to have a Kubernetes cluster.  The Terraform uses _modules_ to construct a K8s cluster

#### Create GKE Cluster

NOTE:  Adjust variable numbers for different sized cluster and nodes.  Default node pool and additional node poll are created by Terraform module.

```bash
# Initialize Terraform
user@domain:~$ terraform init
# Validate plan
user@domain:~$ terraform plan
# Build infrastructure (respond with explicit 'yes')
user@domain:~$ terraform apply
```

------
------

# Connect to Cluster

> Connecting to the Cluster should be performed by ALL.  The GKE cluster MUST exist before connecting.

Once the cluster has been created, each participant will need to configure `kubectl` to authenticate to the new cluster. Using GKE, this process is simple and straight forward using a `gcloud` command.

## Create kubectl configuration

Utilize gcloud's special command to capture output into `kubectl`

```bash
# Example with defaults from Terraform Services GKE module
gcloud container clusters get-credentials prod-master --zone=us-west1-a
```

### Validate

```bash
# View current config name
kubectl config current-context
# Review the current configuration
kubectl config view
## Results in something similar to
gke_kubernetes-bootcamp-project_us-west1-a_prod-master
```

> **NOTE:  Configuring multiple contexts is not covered in the Primary Bootcamp**

```bash
$ kubectl config set-context dev --namespace=development --cluster=dev-master --user=<::::::::>
$ kubectl config set-context prod --namespace=production --prod-master --user=<::::::::>
```
